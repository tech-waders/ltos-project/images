FROM debian:bookworm

RUN cp /etc/apt/sources.list /etc/apt/sources.list~; \
  echo "deb-src http://deb.debian.org/debian bookworm main contrib" >> /etc/apt/sources.list; \
  apt update; \
  apt install -y rsync wget tar texinfo; \
  apt-get build-dep -y gcc-defaults binutils
